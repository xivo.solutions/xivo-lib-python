#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo',
    version='1.0',
    description='These are useful python libraries to be used with XIVO code',
    author='Avencall',
    author_email='xivo-dev@lists.proformatique.com',
    url='http://projects.xivo.io/',
    packages=find_packages(),
)
