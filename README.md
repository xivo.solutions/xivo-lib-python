# xivo-lib-python

xivo-lib-python is a common library used by various other services in XiVO

## Running unit tests

Launch them in container.
See [randd wiki](https://gitlab.com/avencall/randd-doc/-/wikis/build/dev-pbx-setup#launch-unit-tests-of-project)

## Running integration tests

The tests will be launced in docker.

But first you need to build the environment (dockers) needed for the integration test.

You need Docker installed.

1. First install the requirements :
    ```bash
    cd integration_tests
    ```
1. Then build the test setup:
    ```bash
    make test-setup
    ```
1. And finally run the test
    ```bash
    make test
    ```
1. You can clean everything with
    ```bash
    make distclean
    ```

### Interaction with other projects

These test depends on rabbitmq. By default it will be run agains `master` branch of rabbitmq.

If you need to rely on another branch do the following:

```bash
RABBITMQ_TEST_BRANCH=2021.15
make test-setup
make test
```