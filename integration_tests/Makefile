.PHONY: test-setup test distclean

LTS_CODENAME = $(shell dpkg-parsechangelog --show-field distribution -l ../debian/changelog | sed 's/^xivo-//' )
COMPOSE_FILES = $(wildcard assets/*/docker-compose.yml)

RABBITMQ_TEST_BRANCH ?= master
RABBITMQ_DIR=./rabbitmq
RABBITMQ_DOCKER=$(RABBITMQ_DIR)/contribs/rabbitmq-test/Dockerfile

DOCKER_TAG=lib-python_integration_tests

test-setup : test-setup-local

test-setup-local: build-rabbit build-service
	docker pull dokku/wait
	docker pull consul:0.7.1

build-rabbit:
	rm -rf $(RABBITMQ_DIR)
	git clone --depth 1 -b $(RABBITMQ_TEST_BRANCH) "git@gitlab.com:xivo.solutions/rabbitmq.git"
	docker build --no-cache -t xivoxc/rabbitmq:$(DOCKER_TAG) $(RABBITMQ_DIR)/docker
	docker build --no-cache -t xivoxc/rabbitmq-test --build-arg VERSION=$(DOCKER_TAG) -f ${RABBITMQ_DOCKER} ${RABBITMQ_DIR}

build-service:
	docker build -t myservice -f assets/common/Dockerfile ..

test:
	docker run --rm \
	 	-v /var/run/docker.sock:/var/run/docker.sock \
		-v /etc/localtime:/etc/localtime:ro \
		-v /etc/timezone:/etc/timezone:ro \
		-v /etc/group:/etc/group:ro \
		-v /etc/passwd:/etc/passwd:ro \
		-v /etc/shadow:/etc/shadow:ro \
		-v "`pwd`":"`pwd`" \
		-w "`pwd`" \
		--network host \
		xivoxc/debian-builder-$(LTS_CODENAME):latest \
		/bin/bash -c "python3 -m venv /tmp/venv3; \
			source /tmp/venv3/bin/activate; \
			pip install --upgrade pip; \
			pip install -r test-requirements.txt; \
			python3 -m unittest -v"

run-docker-builder:
	docker run --rm -it \
	 	-v /var/run/docker.sock:/var/run/docker.sock \
		-v /etc/localtime:/etc/localtime:ro \
		-v /etc/timezone:/etc/timezone:ro \
		-v /etc/group:/etc/group:ro \
		-v /etc/passwd:/etc/passwd:ro \
		-v /etc/shadow:/etc/shadow:ro \
		-v "`pwd`":"`pwd`" \
		-w "`pwd`" \
		--network host \
		xivoxc/debian-builder-$(LTS_CODENAME):latest

stop:
	@$(foreach cpf,$(COMPOSE_FILES),$(shell docker compose -f $(cpf) kill))

clean:
	@$(foreach cpf,$(COMPOSE_FILES),$(shell docker compose -f $(cpf) down))

distclean: stop clean
	docker rmi -f xivoxc/rabbitmq:$(DOCKER_TAG)
	docker rmi -f xivoxc/rabbitmq-test
	docker rmi -f myservice
	rm -rf $(RABBITMQ_DIR)