git+https://gitlab.com/xivo.solutions/xivo-test-helpers
kombu==5.2.4
pyhamcrest
py-consul==1.3.0
future
pyyaml==6.0
requests==2.31.0
