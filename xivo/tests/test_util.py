# -*- coding: utf-8 -*-

from unittest import TestCase

from hamcrest import assert_that, equal_to

from xivo.util import delete_chars, remove_diacritics, tokenize


class TestUtil(TestCase):
    def test_delete_chars(self):
        assert_that(delete_chars('meow black cat', 'aeiou'), equal_to('mw blck ct'))

    def test_remove_diacritics(self):
        test_string_lower = 'příliš žluťoučký kůň úpěl ďábelské ódy'
        test_string_upper = 'PŘÍLIŠ ŽLUŤOUČKÝ KŮŇ ÚPĚL ĎÁBELSKÉ ÓDY'
        result_string_lower = 'prilis zlutoucky kun upel dabelske ody'
        result_string_upper = 'PRILIS ZLUTOUCKY KUN UPEL DABELSKE ODY'
        assert_that(remove_diacritics(test_string_lower), equal_to(result_string_lower))
        assert_that(remove_diacritics(test_string_upper), equal_to(result_string_upper))

    def test_tokenize(self):
        test_string = 'příliš žluťoučký kůň úpěl ďábelské ódy'
        result_string = ['prilis', 'zlutoucky', 'kun', 'upel', 'dabelske', 'ody']
        assert_that(tokenize(test_string), equal_to(result_string))
