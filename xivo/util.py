# -*- coding: utf-8 -*-


import re
import unicodedata


def delete_chars(charstring, chars):
    """
    Deletes every character contained in chars (str) from charstring (str).
    """
    return ''.join([_ for _ in charstring if _ not in chars])


def remove_diacritics(input_str):
    """
    Removes diacritic from input_str
    """
    if not input_str:
        return u''
    else:
        text = input_str
        if not isinstance(text, str):
            text = str(text, 'utf-8')

        res = u''.join([_ for _ in unicodedata.normalize('NFKD', text) if not unicodedata.combining(_)])
        return res


def tokenize(input_str):
    """
    Splits input_str by ' ', '-', '+', ',' and remove diacritic from it
    """
    return [_ for _ in re.split('[- +,]', remove_diacritics(input_str).lower()) if _]
